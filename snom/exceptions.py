class SnomException(Exception):
    pass


class UnsupportedAction(SnomException):
    pass


class InvalidAuthenticationMethod(SnomException):
    pass


class ActionFailed(SnomException):
    pass