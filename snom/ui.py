import argparse
import re
import sys
try:
    from urllib.parse import unquote
except ImportError:
    from urllib import unquote

import six
from six.moves import tkinter
from six.moves import tkinter_messagebox

from snom.cli import main as cli_main
from snom.connection import AuthMethod
from snom.conf import DEFAULT_DEVICE
from snom.conf import settings
from snom.exceptions import ActionFailed
from snom.exceptions import UnsupportedAction


ENCODING = 'utf-8'
URI_PREFIXES = re.compile(r'^(snom|tel):(//)?', re.I)
auth_methods = (
    (AuthMethod.NONE, u'None'),
    (AuthMethod.BASIC, u'Basic'),
    (AuthMethod.DIGEST, u'Digest')
    )


def u(val):
    # ``six.u`` doesn't allow us to specify an encoding
    decode = False
    if six.PY3:
        if isinstance(val, bytes):
            decode = True
    else:
        if isinstance(val, str):
            decode = True
    if decode:
        return val.decode(ENCODING)
    return val


def error(title, message, parent):
    tkinter_messagebox.showerror(title, message, parent=parent)


class DiallerView(tkinter.Tk):

    keys_view = None
    config_view = None
    device_name = None
    device = None
    connection = None
    number_value = None

    def __init__(self, device_name=None, number=None, *args, **kwargs):
        tkinter.Tk.__init__(self, *args, **kwargs)
        self.wm_title(u'Snom dialler')
        self.create_widgets(number)
        if self.device_name is None:
            device = settings.default_device
        elif settings.has_device(self.device_name):
            device = settings.get_device(self.device_name)
        else:
            # Invalid device name.
            raise ValueError(u'No such device, {0}'.format(self.device_name))
        if device is None:
            # No default device
            self.show_config(device_name=DEFAULT_DEVICE,
                             device_name_editable=False)
        else:
            self.device = device
            self.connection = device.connection()

    def create_widgets(self, number=None):
        box = tkinter.Frame(self)
        box.pack()
        number = number or u''
        self.number_value = tkinter.StringVar(value=number)
        tkinter.Entry(box, textvariable=self.number_value).pack({'side': 'left'})
        tkinter.Button(box, text=u'\u260e', command=self.dial).pack({'side': 'left'})
        tkinter.Button(box, text=u'\u283f', command=self.show_keys).pack({'side': 'left'})
        tkinter.Button(box, text=u'\u2699', command=self.show_config).pack({'side': 'left'})

    def dial(self):
        number_val = u(self.number_value.get())
        number = number_val.replace(u'-', u'')
        number = number.replace(u'.', u'')
        number = number.replace(u' ', u'')
        if re.search(r'[^0-9*#]', number):
            error(
                u'Invalid number',
                u'Invalid telephone number, {0}'.format(number_val),
                self)
            return
        try:
            self.connection.dial(number)
        except ActionFailed:
            error(u'Dialling failed', u'Dialling failed', self)
        except UnsupportedAction:
            error(u'Dialling unsupported',
                  u'Dialling unsupported on this device', self)

    def show_keys(self):
        if self.keys_view is not None:
            self.keys_view.destroy()
        keys_view_cls = device_views.get(self.device.get('model'))
        if keys_view_cls:
            self.keys_view = keys_view_cls(self.connection, master=self)
        else:
            error(u'Unsupport model', u'Model not supported', self)

    def show_config(self, device=None, device_name=None,
                    device_name_editable=True):
        if device is None:
            device = self.device
        if device and device_name is None:
            device_name = device.name
        if self.config_view:
            self.config_view.destroy()
        self.config_view = DeviceConfiguration(
            device, device_name, device_name_editable, master=self)
        self.config_view.transient(self)
        self.config_view.grab_set()
        self.wait_window(self.config_view)
        if self.config_view.device:
            self.device = self.config_view.device
            self.device_name = self.config_view.device.name
            self.connection = self.config_view.device.connection()
        else:
            self.destroy()

    def keys_view_closed(self):
        self.keys_view = None


class KeysView(tkinter.Toplevel):

    def __init__(self, connection, *args, **kwargs):
        tkinter.Toplevel.__init__(self, *args, **kwargs)
        self.wm_title(u'Snom {0}'.format(connection.model))
        self.connection = connection
        self.grid()
        self.create_widgets()
        self.bind_keys()

    def create_widgets(self):
        pass

    def bind_keys(self):
        self.bind('<Key>', self.handle_keypress)

    def maybe_perform_action(self, action):
        if not self.connection.action_supported(action):
            print('Not supported: {0}'.format(action))
            return
        try:
            self.connection.action_command(action)
        except ActionFailed:
            error(u'Action failed', u'Action failed', self)

    def handle_keypress(self, event):
        char = event.char.lower()
        if char == '1':
            self.maybe_perform_action('ONE')
        elif char in ('2abc'):
            self.maybe_perform_action('TWO')
        elif char in ('3def'):
            self.maybe_perform_action('THREE')
        elif char in ('4ghi'):
            self.maybe_perform_action('FOUR')
        elif char in ('5jkl'):
            self.maybe_perform_action('FIVE')
        elif char in ('6mno'):
            self.maybe_perform_action('SIX')
        elif char in ('7pqrs'):
            self.maybe_perform_action('SEVEN')
        elif char in ('8tuv'):
            self.maybe_perform_action('EIGHT')
        elif char in ('9wxyz'):
            self.maybe_perform_action('NINE')
        elif char in ('0'):
            self.maybe_perform_action('ZERO')
        elif char == '<Return>':
            self.maybe_perform_action('ENTER')
        elif char == '<Left>':
            self.maybe_perform_action('LEFT')
        elif char == '<Right>':
            self.maybe_perform_action('RIGHT')
        elif char == '<Up>':
            self.maybe_perform_action('UP')
        elif char == '<Down>':
            self.maybe_perform_action('DOWN')
        elif char == '+':
            self.maybe_perform_action('VOLUME_UP')
        elif char == '-':
            self.maybe_perform_action('VOLUME_DOWN')

    def action_handler(self, action):
        def inner():
            try:
                self.connection.action_command(action)
            except ActionFailed:
                error(u'Action failed', u'Action failed', self)

        return inner

    def dial(self, number):
        self.connection.dial(number)

    def action_button(self, label, action):
        return tkinter.Button(
            self, text=label, command=self.action_handler(action))

    def destroy(self, *args, **kwargs):
        self.master.keys_view_closed()
        tkinter.Toplevel.destroy(self, *args, **kwargs)


class Snom300View(KeysView):

    def create_widgets(self):
        self.action_button(u'\u2b06', 'UP').grid(
            row=0, column=2)
        self.action_button(u'\u2b05', 'LEFT').grid(
            row=1, column=1)
        self.action_button(u'\u27a1', 'RIGHT').grid(
            row=1, column=3)
        self.action_button(u'\u2b07', 'DOWN').grid(
            row=2, column=2)
        self.action_button(u'\u2714', 'ENTER').grid(
            row=1, column=4)
        self.action_button(u'\u2718', 'CANCEL').grid(
            row=1, column=0)
        self.action_button(u'-', 'VOLUME_DOWN').grid(
            row=4, column=0)
        self.action_button(u'+', 'VOLUME_UP').grid(
            row=4, column=1)
        self.action_button(u'SPK', 'SPEAKER').grid(
            row=4, column=2, columnspan=2)
        self.action_button(u'F1', 'F1').grid(
            row=4, column=4)
        self.action_button(u'F2', 'F2').grid(
            row=5, column=4)
        self.action_button(u'F3', 'F3').grid(
            row=6, column=4)
        self.action_button(u'F4', 'F4').grid(
            row=7, column=4)
        self.action_button(u'TRANS', 'TRANSFER').grid(
            row=8, column=4, columnspan=2)
        self.action_button(u'DND', 'DND').grid(
            row=9, column=4, columnspan=2)
        self.action_button(u'1', 'ONE').grid(
            row=6, column=0)
        self.action_button(u'2', 'TWO').grid(
            row=6, column=1)
        self.action_button(u'3', 'THREE').grid(
            row=6, column=2)
        self.action_button(u'4', 'FOUR').grid(
            row=7, column=0)
        self.action_button(u'5', 'FIVE').grid(
            row=7, column=1)
        self.action_button(u'6', 'SIX').grid(
            row=7, column=2)
        self.action_button(u'7', 'SEVEN').grid(
            row=8, column=0)
        self.action_button(u'8', 'EIGHT').grid(
            row=8, column=1)
        self.action_button(u'9', 'NINE').grid(
            row=8, column=2)
        self.action_button(u'0', 'ZERO').grid(
            row=9, column=1)


class Snom320View(Snom300View):

    pass


class Snom360View(Snom320View):

    pass


class Snom370View(Snom360View):

    pass


class Snom710View(KeysView):

    pass


class Snom715View(Snom710View):

    pass


class SnomD715View(Snom715View):

    pass


class Snom720View(Snom710View):

    pass


class Snom725View(Snom720View):

    pass


class Snom760View(Snom720View):

    pass


class SnomD7View(Snom760View):

    pass


class Snom820View(Snom760View):

    pass


class Snom821View(Snom820View):

    pass


class Snom870View(Snom820View):

    pass


device_views = {
    '300': Snom300View,
    '320': Snom320View,
    '360': Snom360View,
    '370': Snom370View,
    '710': Snom710View,
    '715': Snom715View,
    'D715': SnomD715View,
    '720': Snom720View,
    '725': Snom725View,
    '760': Snom760View,
    'D7': SnomD7View,
    '820': Snom820View,
    '821': Snom821View,
    '870': Snom870View,
    }


class DeviceConfiguration(tkinter.Toplevel):

    def __init__(self, device=None, device_name=None,
                 device_name_editable=True, *args,
                 **kwargs):
        tkinter.Toplevel.__init__(self, *args, **kwargs)
        self.wm_title(u'Snom device configuration')
        self.device = device
        self.device_name = device_name
        self.device_name_editable = device_name_editable
        self.create_widgets()

    def create_widgets(self):
        if self.device is None:
            tkinter.Label(self, text=u'Name').grid(row=0)
            self.name = tkinter.StringVar(value=self.device_name)
            name = tkinter.Entry(self, textvariable=self.name)
            name.grid(row=0, column=1, columnspan=2)
            if not self.device_name_editable:
                name.config(state=tkinter.DISABLED)
        tkinter.Label(self, text=u'Model').grid(row=1)
        self.model = tkinter.Listbox(self, exportselection=0)
        idx = 0
        for model_name in sorted(device_views.keys()):
            self.model.insert(tkinter.END, model_name)
            if self.device and model_name == self.device.get('model'):
                self.model.selection_set(first=idx)
            idx += 1
        self.model.grid(row=1, column=1, columnspan=2)
        tkinter.Label(self, text=u'Firmware version').grid(row=2)
        self.version = tkinter.StringVar()
        if self.device:
            self.version.set(self.device.get('version'))
        version = tkinter.Entry(self, textvariable=self.version)
        version.grid(row=2, column=1, columnspan=2)
        tkinter.Label(self, text=u'Hostname / IP').grid(row=3)
        self.host = tkinter.StringVar()
        if self.device:
            self.host.set(self.device.get('host'))
        host = tkinter.Entry(self, textvariable=self.host)
        host.grid(row=3, column=1, columnspan=2)
        tkinter.Label(self, text=u'Username').grid(row=4)
        self.username = tkinter.StringVar()
        if self.device:
            self.username.set(self.device.get('username'))
        username = tkinter.Entry(self, textvariable=self.username)
        username.grid(row=4, column=1, columnspan=2)
        tkinter.Label(self, text=u'Password').grid(row=5)
        self.password = tkinter.StringVar()
        if self.device:
            self.password.set(self.device.get('password'))
        password = tkinter.Entry(self, textvariable=self.password,
                                 show=u'\u2022')
        password.grid(row=5, column=1, columnspan=2)
        tkinter.Label(self, text=u'Authentication').grid(row=6)
        self.authentication = tkinter.Listbox(self, exportselection=0)
        idx = 0
        for t, l in auth_methods:
            self.authentication.insert(tkinter.END, l)
            if self.device and t == self.device.get('authentication'):
                self.authentication.selection_set(first=idx)
            idx += 1
        self.authentication.grid(row=6, column=1, columnspan=2)
        box = tkinter.Frame(self)
        box.grid(row=7, column=1, columnspan=2)
        w = tkinter.Button(box, text="Save", width=10, command=self.save,
                           default=tkinter.ACTIVE)
        w.pack(side=tkinter.LEFT, padx=5, pady=5)
        w = tkinter.Button(box, text="Cancel", width=10, command=self.destroy)
        w.pack(side=tkinter.LEFT, padx=5, pady=5)

        self.bind("<Return>", self.save)
        self.bind("<Escape>", lambda event: self.destroy())

    def save(self):
        model = self.model.get(tkinter.ACTIVE)
        version = u(self.version.get())
        host = u(self.host.get())
        username = u(self.username.get())
        password = u(self.password.get())
        authentication = {
                u'None': AuthMethod.NONE,
                u'Basic': AuthMethod.BASIC,
                u'Digest': AuthMethod.DIGEST,
                }.get(u(self.authentication.get(tkinter.ACTIVE)))
        if self.device:
            self.device['model'] = model
            self.device['version'] = version
            self.device['host'] = host
            self.device['username'] = username
            self.device['password'] = password
            self.device['authentication_type'] = authentication
        else:
            name = u(self.name.get())
            self.device = settings.add_device(name, host, model, version,
                                              username, password,
                                              authentication)
        settings.save()
        self.destroy()


def main(argv=None):
    if argv is None:
        argv = sys.argv[1:]
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', action='store_true', dest='interactive', default=False)
    parser.add_argument('-c', action='append', dest='commands',
                        default=[])
    parser.add_argument('-d', dest='device_name')
    parser.add_argument('number', nargs='?')
    options = parser.parse_args(argv)

    if options.interactive or options.commands:
        return cli_main(argv)
    else:
        number = options.number
        if number:
            number = unquote(URI_PREFIXES.sub('', number))
        app = DiallerView(options.device_name, number)
        app.tk.call('encoding', 'system', ENCODING)
        app.mainloop()



if __name__ == '__main__':
    main()
