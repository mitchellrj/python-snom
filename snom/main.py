try:
    from six.moves import tkinter
except (ImportError, OSError):
    HAVE_TK = False
else:
    HAVE_TK = True


def main(argv=None):
    if HAVE_TK:
        from snom.ui import main as ui_main

        return ui_main(argv)
    else:
        from snom.cli import main as cli_main

        return cli_main(argv)


if __name__ == '__main__':
    main()

