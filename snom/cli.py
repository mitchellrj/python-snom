import argparse
import shlex
import sys

from snom.conf import settings
from snom.connection import AuthMethod


auth_methods = (AuthMethod.BASIC, AuthMethod.DIGEST, AuthMethod.NONE)


class CLI(object):

    def __init__(self, stdin=None, stdout=None, stderr=None):
        if stdin is None:
            stdin = sys.stdin
        if stdout is None:
            stdout = sys.stdout
        if stderr is None:
            stderr = sys.stderr
        self.stdin = stdin
        self.stdout = stdout
        self.stderr = stderr
        self.connection = settings.default_device.connection()

    def interact(self):
        while 1:
            try:
                prompt = u'> '
                try:
                    line = self.raw_input(prompt)
                    encoding = getattr(self.stdin, "encoding", None)
                    if encoding and not isinstance(line, unicode):
                        line = line.decode(encoding)
                except EOFError:
                    self.write(u"\n")
                    break
                else:
                    if self.push(line):
                        break
            except KeyboardInterrupt:
                self.error(u"\nKeyboardInterrupt\nType `exit` or `quit`.\n")

    def raw_input(self, prompt=None):
        self.stdout.write(prompt)
        c = ''
        buf = ''
        while 1:
            c = self.stdin.read(1)
            if c == '\n':
                break
            buf += c

        return buf

    def write(self, data):
        encoding = getattr(self.stdout, 'encoding', 'UTF-8')
        self.stdout.write(data.encode(encoding))

    def error(self, data):
        encoding = getattr(self.stdout, 'encoding', 'UTF-8')
        self.stderr.write(data.encode(encoding))

    def push(self, line):
        ops = shlex.split(line)
        oper = ops[0]
        operands = ops[1:]
        if oper == u'new':
            if len(operands) < 4 or len(operands) > 7:
                self.error(
                    u'Invalid number of arguments. Type `help` for a list of '
                    u'commands.\n')
                return False
            name, host, model, version = operands[:4]
            username = password = authentication_method = None
            if not settings.has_device(name):
                self.error(u'Device already exists with that name.\n')
                return False
            if len(operands) > 4:
                username = operands[4]
            if len(operands) > 5:
                password = operands[4]
            if len(operands) > 6:
                authentication_method = operands[4]
                if authentication_method not in auth_methods:
                    self.error(
                        u'Invalid authentication method. Must be one of '
                        u'{0}.\n'.format(
                            u', '.join(map(repr, auth_methods))
                            ))
                    return False
            settings.add_device(name, host, model, version, username,
                                password, authentication_method)

        elif oper == u'del':
            if len(operands) != 1:
                self.error(
                    u'Invalid number of arguments. Type `help` for a list of '
                    u'commands.\n')
                return False
            device = operands[0]
            self.connection = settings.delete_device(device)
        elif oper == u'save':
            if operands:
                self.error(
                    u'Invalid number of arguments. Type `help` for a list of '
                    u'commands.\n')
                return False
            settings.save()
        elif oper in (u'exit', u'quit'):
            if operands:
                self.error(
                    u'Invalid number of arguments. Type `help` for a list of '
                    u'commands.\n')
                return False
            return True
        elif oper == u'list':
            if operands:
                self.error(
                    u'Invalid number of arguments. Type `help` for a list of '
                    u'commands.\n')
                return False
            for device in settings.devices():
                self.write(u'{0}\n'.format(device))
        elif oper == u'use':
            if len(operands) != 1:
                self.error(
                    u'Invalid number of arguments. Type `help` for a list of '
                    u'commands.\n')
                return False
            device = operands[0]
            if not settings.has_device(device):
                self.error(u'No such device.\n')
                return False
            self.connection = settings.get_device(device).connection()
        elif oper in (u'help', u'h', u'?'):
            self.write(u'\n'.join([
                u'Commands:',
                u'list: list available devices',
                u'use name: use the device named `name`',
                (
                    u'add name host model version '
                    u'[username password authentication_method]: add a new '
                    u'device'
                ),
                u'del name: delete the device named `name`',
                u'save: save the device configuration',
                u'help: this help',
                u'exit: exit',
                u'buttons: list the available buttons',
                (
                    u'button key [time [pause]]: push button `key` for '
                    u'`time` milliseconds and then pause for `pause` '
                    u'milliseconds'
                ),
                (
                    u'dial number [as]: dial `number` as the identity `as` '
                    u'or the default identity'
                ),
                (
                    u'ring [ringer]: make the device ring `ringer` or ringer '
                    u'1.'
                ),
                (
                    u'tone tone: send the DTMF tone, `tone`.'
                ),
                ]) + u'\n')
        elif oper == u'buttons':
            for action, description in self.connection.actions.items():
                self.write('{0}: {1}\n'.format(action, description))
        elif oper == u'button':
            if self.connection is None:
                self.error(u'You must select a connection to use first.\n')
                return False
            if not operands or len(operands) > 3:
                self.error(
                    u'Invalid number of arguments. Type `help` for a list of '
                    u'commands.\n')
                return False
            action = operands[0]
            if len(operands) > 1:
                time = operands[1]
            if len(operands) > 2:
                pause = operands[2]
            if not self.connection.action_supported(action):
                self.error(u'Action not supported by this device.\n')
                return False
            self.connection.action_command(action, time, pause)
        elif oper == u'dial':
            if self.connection is None:
                self.error(u'You must select a connection to use first.\n')
                return False
            if not operands or len(operands) > 2:
                self.error(
                    u'Invalid number of arguments. Type `help` for a list of '
                    u'commands.\n')
                return False
            number = operands[0]
            identity = None
            if len(operands) == 2:
                identity = operands[1]
            if not self.connection.dial_supported():
                self.error(u'Action not supported by this device.\n')
                return False
            self.connection.dial(number, identity)
        elif oper == u'ring':
            if self.connection is None:
                self.error(u'You must select a connection to use first.\n')
                return False
            if len(operands) > 1:
                self.error(
                    u'Invalid number of arguments. Type `help` for a list of '
                    u'commands.\n')
                return False
            ringer = u'1'
            if len(operands) == 1:
                ringer = operands[0]
            if not self.connection.play_ringer_supported():
                self.error(u'Action not supported by this device.\n')
                return False
            self.connection.play_ringer(ringer)
        elif oper == u'tone':
            if self.connection is None:
                self.error(u'You must select a connection to use first.\n')
                return False
            if not operands or len(operands) > 1:
                self.error(
                    u'Invalid number of arguments. Type `help` for a list of '
                    u'commands.\n')
                return False
            tone = operands[0]
            if not self.connection.send_tones_supported():
                self.error(u'Action not supported by this device.\n')
                return False
            self.connection.send_tones(tone)
        else:
            self.error(
                u'Unrecognized command. Type `help` for a list of commands.\n')
            return False


def main(argv=None, exit_=None):
    if argv is None:
        argv = sys.argv[1:]
    if exit_ is None:
        exit_ = sys.exit
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', dest='commands', default=[], action='append')
    parser.add_argument('-i', dest='ignore', action="store_const", const=None)
    options = parser.parse_args(argv)
    cli = CLI()
    if options.commands:
        for cmd in options.commands:
            result = cli.push(cmd)
            if result is False:
                return exit_(1)
    else:
        cli.interact()
    return exit_(0)


if __name__ == '__main__':
    main()
