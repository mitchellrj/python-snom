from collections import Iterable
import operator
try:
    from urllib.parse import urlunparse
except ImportError:
    from urlparse import urlunparse

from defusedxml.ElementTree import fromstring
import requests
from requests.auth import HTTPBasicAuth
from requests.auth import HTTPDigestAuth
import six
from six.moves import UserDict

from snom import exceptions


DESKTOP_MODELS = (
    '300',
    '320',
    '360',
    '370',
    '710',
    '715',
    'D715',
    '720',
    '725',
    '760',
    'D7',
    '820',
    '821',
    '870',
    )


class AuthMethod:

    NONE = 'NONE'
    BASIC = 'BASIC'
    DIGEST = 'DIGEST'


class Action(UserDict):

    _base_actions = {
        'CANCEL': u'Cancel',
        'ENTER': u'Enter',
        'OFFHOOK': u'Lift handset',
        'ONHOOK': u'Hang up handset',
        'DISCONNECT': u'Disconnect',
        'CLEAR': u'Clear',
        'FUNCTION': u'Function',
        'RECALL': u'Recall',
        'RIGHT': u'Right',
        'LEFT': u'Left',
        'UP': u'Up',
        'DOWN': u'Down',
        'VOLUME_UP': u'Volume up',
        'VOLUME_DOWN': u'Volume down',
        'MENU': u'Menu',
        'REDIAL': u'Redial',
        'DND': u'Do not disturb',
        'REC': u'Record',
        'F1': u'Function key 1',
        'F2': u'Function key 2',
        'F3': u'Function key 3',
        'F4': u'Function key 4',
        'SPEAKER': u'Speaker',
        'HEADSET': u'Headset',
        'TRANSFER': u'Transfer',
        'HOLD': u'Hold',
        'ZERO': u'0',
        'ONE': u'1',
        'TWO': u'2',
        'THREE': u'3',
        'FOUR': u'4',
        'FIVE': u'5',
        'SIX': u'6',
        'SEVEN': u'7',
        'EIGHT': u'8',
        'NINE': u'9',
        'P1': u'Programmable key 1',
        'P2': u'Programmable key 2',
        'P3': u'Programmable key 3',
        'P4': u'Programmable key 4',
        'P5': u'Programmable key 5',
        'P6': u'Programmable key 6',
        'P7': u'Programmable key 7',
        'P8': u'Programmable key 8',
        'P9': u'Programmable key 9',
        'P10': u'Programmable key 10',
        'P11': u'Programmable key 11',
        'P12': u'Programmable key 12',
        'P13': u'Programmable key 13',
        'P14': u'Programmable key 14',
        'P15': u'Programmable key 15',
        # TODO: expansion module
        # 'Expansion module key 0': 'E0',
    }
    _excludes = {
        'F1': ['870'],
        'F2': ['870'],
        'F3': ['870'],
        'F4': ['190', '200', '870'],
        }
    _includes = {
        'FUNCTION': ['190', '200'],
        'RECALL': ['190', '200', '220'],
        'CLEAR': ['190', '200', '220'],
        'DISCONNECT': ['190', '200', '220'],
        'P1': ['190', '200', '220', '300', '320', '360', '370', '820', '870'],
        'P2': ['190', '200', '220', '300', '320', '360', '370', '820', '870'],
        'P3': ['190', '200', '220', '300', '320', '360', '370', '820', '870'],
        'P4': ['190', '200', '220', '300', '320', '360', '370', '820', '870'],
        'P5': ['190', '200', '220', '320', '360', '370', '820', '870'],
        'P6': ['320', '360', '370', '820', '870'],
        'P7': ['320', '360', '370', '820', '870'],
        'P8': ['320', '360', '370', '820', '870'],
        'P9': ['320', '360', '370', '820', '870'],
        'P10': ['320', '360', '370', '820', '870'],
        'P11': ['320', '360', '370', '820', '870'],
        'P12': ['320', '360', '370', '820', '870'],
        'P13': ['870'],
        'P14': ['870'],
        'P15': ['870'],
        }

    def __init__(self, model, firmware_version):
        UserDict.__init__(self)
        for k, v in self._base_actions.items():
            if model in self._excludes.get(k, []):
                continue
            if model not in self._includes.get(k, [model]):
                continue

            self[k] = v
            setattr(self, k, v)


class Key(UserDict):

    _base_keys = {
        'HOLD': 'F_HOLD',
        'ZERO': u'0',
        'ONE': u'1',
        'TWO': u'2',
        'THREE': u'3',
        'FOUR': u'4',
        'FIVE': u'5',
        'SIX': u'6',
        'SEVEN': u'7',
        'EIGHT': u'8',
        'NINE': u'9',
    }
    _version_changes = {
        (operator.lt, (8, 7, 3, 7)): {'HOLD': 'F_R'}
    }
    _model_changes = {
    }

    def __init__(self, actions, model, firmware_version):
        UserDict.__init__(self)
        changes = {}
        changes.update(self._base_keys)
        for (op, target_version), v_c in self._version_changes.items():
            if op(firmware_version, target_version):
                changes.update(v_c)
        for target_models, m_c in self._model_changes.items():
            if model in target_models:
                changes.update(m_c)
        for k in actions.keys():
            if k in changes:
                v = changes[k]
            else:
                v = k
            self[k] = v
            setattr(self, k, v)


class Contact(object):

    nick = None
    number = None
    number_type = None
    first_name = None
    last_name = None
    email = None
    title = None
    organization = None
    note = None
    group = None
    favorite = False
    context = None
    type = None
    index = None

    def __init__(self, name=None, **kwargs):
        self.nick = name
        for k, v in kwargs.items():
            setattr(self, k, v)

    @property
    def name(self):
        result = u''
        if self.first_name:
            result = self.first_name
        if self.last_name:
            if result:
                result += ' '
            result += self.last_name
        return result

    def __str__(self):
        if self.nick:
            return self.nick
        return self.name or self.number or ''

    def __repr__(self):
        s = self.__unicode__()
        if s:
            s = u' {0}'.format(repr(s))
        return u'<{0}{1}>'.format(self.__class__.__name__, s)

    if six.PY2:
        __unicode__ = __str__


class SnomConnection(object):

    key_class = Key
    action_class = Action
    version = None
    model = None

    def __init__(self, host, model, version, username=None, password=None,
                 auth_method=None, key_class=None,
                 action_class=None):
        self.host = host
        self._session = requests.Session()
        self._get_model_info(model, version)
        if key_class is not None:
            self.key_class = key_class
        if action_class is not None:
            self.action_class = action_class
        if auth_method == AuthMethod.BASIC:
            self._auth = HTTPBasicAuth(username, password)
        elif auth_method == AuthMethod.DIGEST:
            self._auth = HTTPDigestAuth(username, password)
        elif auth_method in (None, 'NONE', '', u'', AuthMethod.NONE):
            self._auth = None
        else:
            raise exceptions.InvalidAuthenticationMethod(auth_method)

    def make_request(self, path, params=None, method=None):
        if method is None:
            method = 'get'
        uri = urlunparse((
            'http',
            self.host,
            path,
            '',
            '',
            ''
            ))
        resp = self._session.request(method, uri, params=params,
                                     auth=self._auth)
        try:
            resp.raise_for_status()
        except requests.HTTPError as e:
            six.raise_from(exceptions.ActionFailed(), e)
        return resp.text

    def is_desktop(self, model=None):
        if model is None:
            model = self.model
        return model in DESKTOP_MODELS

    def _get_model_info(self, model, version_string):
        version = tuple(version_string.split('.'))
        self.actions = self.action_class(model, version)
        self.keys = self.key_class(self.actions, model, version)
        self.model = model
        self.version = version

    def _run_key_commands(self, *keys):
        if not keys:
            return
        key_data = []
        for command in keys:
            if isinstance(command, six.string_types):
                key_data.append(command)
            elif isinstance(command, Iterable):
                key_data.append(u','.join(command))
        self.make_request('command.htm', {u'key': u';'.join(key_data)})

    def action_command(self, action, time=None, pause=None):
        if not self.action_supported(action):
            raise exceptions.UnsupportedAction(action)

        key = self.keys[action]
        if time is not None:
            key = (key, time)
            if pause is not None:
                key += (pause,)
        elif pause is not None:
            key = (key, 1, pause)
        return self._run_key_commands(key)

    def action_supported(self, action):
        return action in self.actions

    def screen_image_url(self):
        if not self.screen_supported():
            raise exceptions.UnsupportedAction()
        return urlunparse((
            'http',
            self.host,
            'screen.bmp',
            '',
            '',
            ''
            ))

    def screen_image(self):
        return self._session.get(self.screen_image_url()).body

    def screen_supported(self):
        if not self.is_desktop():
            return False
        if self.model == '300' and self.version < (8, 7, 3, 7):
            return False
        return True

    def contacts(self):
        if not self.directory_supported():
            raise exceptions.UnsupportedAction()
        xmlfile = self.make_request('tbook.xml')
        doc = fromstring(xmlfile)
        for item in doc.iter('item'):
            values = {
                'favorite':  item.attrib.get('fav', u'false') == u'true',
                'context': item.attrib.get('context', None) or None,
                'type': item.attrib.get('type', None) or None,
                'index': int(item.attrib.get('index', -1) or 0)
                }
            for attr in item.getchildren():
                values[attr.tag] = attr.text
            yield Contact(**values)

    directory_supported = is_desktop

    def dial(self, number, from_identity=None):
        if not self.dial_supported():
            raise exceptions.UnsupportedAction()
        params = {u'number': number}
        if from_identity is not None:
            params[u'outgoing_uri'] = from_identity
        return self.make_request(u'command.htm', params)

    dial_supported = is_desktop

    def play_ringer(self, number):
        if not self.is_desktop():
            raise exceptions.UnsupportedAction()
        return self.make_request('line_login.htm', {
            u'PLAY_RINGER:{0}'.format(number): u'Play Ringer'
            })

    play_ringer_supported = is_desktop

    def send_tones(self, *tones):
        if not self.is_desktop():
            raise exceptions.UnsupportedAction()
        return self.make_request('command.htm', {
            u'key_dtmf': u''.join(tones)
            })

    send_tones_supported = is_desktop

    def end_all_calls(self):
        if not self.end_all_calls_supported():
            raise exceptions.UnsupportedAction()
        return self.make_request('command.htm', {
            u'RELEASE_ALL_CALLS': u'',
            })

    end_all_calls_supported = is_desktop

    def log_off_all_identities(self):
        if not self.log_off_all_identities_supported():
            raise exceptions.UnsupportedAction()
        return self.make_request('command.htm', {
            u'LOGOFFALL': u'',
            })

    log_off_all_identities_supported = is_desktop

    def log_off(self, identity):
        if not self.log_off_supported():
            raise exceptions.UnsupportedAction()
        return self.make_request('command.htm', {
            u'LOGOFFLINE': identity,
            })

    log_off_supported = is_desktop

    def reregister(self, identity):
        if not self.reregister_supported():
            raise exceptions.UnsupportedAction()
        return self.make_request('command.htm', {
            u'REREGISTER': identity,
            })

    reregister_supported = is_desktop

    def reboot(self):
        if not self.reboot_supported():
            raise exceptions.UnsupportedAction()
        return self.make_request('advanced_update.htm', {
            u'reboot': u'Reboot',
            })

    reboot_supported = is_desktop

    def reset(self):
        if not self.reset_supported():
            raise exceptions.UnsupportedAction()
        return self.make_request('advanced_update.htm', {
            u'reset': u'Reset',
            })

    reset_supported = is_desktop

    def clear_dialled_numbers(self):
        if not self.clear_dialled_numbers_supported():
            raise exceptions.UnsupportedAction()
        return self.make_request('index.htm', {
            u'dialeddel': u'0',
            })

    clear_dialled_numbers_supported = is_desktop

    def clear_missed_calls(self):
        if not self.clear_missed_calls():
            raise exceptions.UnsupportedAction()
        return self.make_request('index.htm', {
            u'misseddel': u'0',
            })

    clear_missed_calls_supported = is_desktop

    def clear_received_calls(self):
        if not self.clear_received_calls_supported():
            raise exceptions.UnsupportedAction()
        return self.make_request('index.htm', {
            u'receiveddel': u'0',
            })

    clear_received_calls_supported = is_desktop

    def fix_line_info_layer(self):
        if not self.fix_line_info_layer():
            raise exceptions.UnsupportedAction()
        return self.make_request('command.htm', {
            u'FIX_LIL': u'true',
            })

    fix_line_info_layer_supported = is_desktop

    def upgrade_firmware(self, firmware_uri):
        if not self.upgrade_firmware_supported():
            raise exceptions.UnsupportedAction()
        return self.make_request('dummy.htm', {
            u'swload': u'load',
            u'firmware': firmware_uri,
            })

    upgrade_firmware_supported = is_desktop

    def touchscreen(self, x, y, press=True, release=True):
        if not self.touchscreen_supported():
            raise exceptions.UnsupportedAction()
        return self.make_request('command.htm', {
            u'touch': u'{0} {1} {2}{3}'.format(
                x, y, u'p' if press else u'', u'r' if release else u''
                )
            })

    def touchscreen_supported(self):
        available = False
        if self.model == '870':
            if self.version < (8, 5) and self.version >= (8, 4, 34):
                available = True
            if self.version >= (8, 7, 2):
                available = True
        return available
