import os.path

from six.moves import configparser
from six.moves import UserDict

from snom.connection import SnomConnection


SETTINGS_FILE = '.snom'
DEFAULT_DEVICE = 'device'


class DeviceSettings(UserDict):

    def __init__(self, settings, section, items):
        UserDict.__init__(self, items)
        self.settings = settings
        self.name = section

    def __setitem__(self, k, v):
        self.settings.set(self.name, k, v)
        UserDict.__setitem__(self, k, v)

    def __delitem__(self, k):
        self.settings.remove_option(self.name, k)
        UserDict.__delitem__(self, k)

    def connection(self):
        return SnomConnection(
            host=self['host'],
            model=self['model'],
            version=self['version'],
            username=self['username'],
            password=self['password'],
            auth_method=self['authentication'],
            )


class Settings(configparser.ConfigParser):

    def __init__(self, configfile=None):
        if configfile is None:
            configfile = os.path.join(os.path.expanduser('~'), SETTINGS_FILE)
        self.configfile = configfile
        configparser.ConfigParser.__init__(self, {
            'host': u'',
            'username': u'',
            'password': u'',
            'authentication': u'NONE',
            'model': u'',
            'version': u'',
            })
        if os.path.exists(configfile):
            self.read(configfile)

    def save(self):
        with open(self.configfile, 'w') as fh:
            self.write(fh)

    devices = configparser.ConfigParser.sections

    def add_device(self, name, host, model, version, username=None,
                   password=None, authentication_method=None):
        self.add_section(name)
        self.set(name, 'host', host)
        self.set(name, 'model', model)
        self.set(name, 'version', version)
        self.set(name, 'username', username)
        self.set(name, 'password', password)
        self.set(name, 'authentication', authentication_method)
        return self.get_device(name)

    @property
    def default_device(self):
        return self.get_device(DEFAULT_DEVICE)

    def has_device(self, name):
        return name == configparser.DEFAULTSECT or self.has_section(name)

    def get_device(self, name):
        if not self.has_device(name):
            return None
        return DeviceSettings(self, name, self.items(name))

    def delete_device(self, name):
        return self.remove_section(name)


settings = Settings()
