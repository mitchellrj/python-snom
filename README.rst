============
python-snom
============

Interact with Snom VoIP desktop phones.

Installation
============
::

    git clone https://bitbucket.org/mitchellrj/python-snom.git
    cd python-snom
    virtualenv -p python2.7 --no-site-packages .
    bin/pip install -r requirements.txt

Usage
=====

GUI
---
::

    bin/snom [number]

Commandline
-----------
::

    bin/snom -i

Commands
````````

* ``help``

API
---
::

    from snom.conf import settings

    device = settings.get_device('device')
    conn = device.connection()

