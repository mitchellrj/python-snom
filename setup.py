import sys

from setuptools import find_packages
from setuptools import setup

try:
    # https://bitbucket.org/ronaldoussoren/modulegraph/issue/22/
    from modulegraph.modulegraph import ModuleGraph
    if (
            hasattr(ModuleGraph, '_scan_code')
            and not hasattr(ModuleGraph, 'scan_code')
            ):

        ModuleGraph.scan_code = ModuleGraph._scan_code
    if (
            hasattr(ModuleGraph, '_load_module')
            and not hasattr(ModuleGraph, 'load_module')
            ):

        ModuleGraph.load_module = ModuleGraph._load_module
except ImportError:
    pass


PY33 = sys.version_info >= (3, 3)


version = '0.0.1'
long_description = '\n\n'.join([open(f).read() for f in [
    'README.rst',
    'LICENSE.rst',
    'CHANGELOG.rst',
    ]])
requires = [
    'defusedxml',
    'requests',
    'six',
    ]
build_require = [
    'modulegraph',
    'py2app'
    ]
tests_require = [
    ]
docs_require = [
    'Sphinx',
]
if not PY33:
    tests_require.append('mock')


setup(
    name='snom',
    version=version,
    description='',
    long_description=long_description,
    keywords='snom sip telephony link href',
    author='Richard Mitchell',
    author_email='mitch@awesomeco.de',
    url='https://github.com/mitchellrj/python-snom',
    license='Apache 2',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: End Users/Desktop',
        'Intended Audience :: Information Technology',
        'Intended Audience :: Telecommunications Industry',
        'License :: OSI Approved :: Apache Software License',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.4',
        'Topic :: Communications :: Internet Phone',
        'Topic :: Communications :: Telephony',
        'Topic :: Desktop Environment',
        ],
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=requires,
    tests_require=tests_require,
    test_suite='nose.collector',
    extras_require={
        # Environment shortcuts
        'dev': build_require + docs_require + tests_require,
        'docs': docs_require,
        'test': tests_require,
        'integration': ['python-coveralls'] + tests_require,
    },
    app=['snom/__init__.py'],
    entry_points="""
    [console_scripts]
    snom=snom.main:main
    """,
    py2app={
        'argv_emulation': True,
        'packages': requires,
        'plist': {
            'CFBundleURLTypes': [
                {
                    'CFBundleTypeRole': 'Viewer',
                    'CFBundleURLName': 'python-snom',
                    'CFBundleURLSchemes': ['tel', 'sip']
                }
            ]
        }
    }
)
